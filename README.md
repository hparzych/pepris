# Pepris

Simple tetris-like game.

## About The Project

![Game screenshot](screenshot.png)

The game was created as a quick project to see how much time it takes to make a full game in Python. Thanks to Python's consise syntax the project took 2 days.

### Built With

- Python3
- Pygame

## Getting Started

1. Clone the repo
   ```sh
   git clone https://gitlab.com/hparzych/pepris.git
   ```
2. Run the game
   ```sh
   python3 main.py
   ```

### Prerequisites

Install python3 and pygame.

```sh
apt install python3 python3-pygame # Debian & Ubuntu
```

## License

Distributed under the MIT License. See `LICENSE` for more information.
