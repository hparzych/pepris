"""Display and global state

This file provides the display and scene engine.
"""

import random
import sys
from dataclasses import dataclass

import pygame
from game import GameScene

# Pygame has to be initialized
pygame.init()


# GlobalState gets passed between the display and it's scenes to
# store the global app settings and state
@dataclass
class GlobalState:
    window_width: int = 800
    window_height: int = 600
    color_bg: tuple = (30, 30, 35)
    color_fg: tuple = (230, 230, 240)
    default_font_name: str = "sans-serif"
    fps: int = 60
    display: object = object()
    extra: object = object()


class SimpleTitleScene:
    def __init__(self, **kwargs):
        self.next_scene = kwargs["next_scene"] if "next_scene" in kwargs else None
        self.title_message = (
            kwargs["title_message"] if "title_message" in kwargs else None
        )
        self.message = kwargs["message"] if "message" in kwargs else None

    def resetState(self, globalState):
        self.globalState = globalState
        self.font = pygame.font.SysFont(self.globalState.default_font_name, 24)
        self.title_font = pygame.font.SysFont(self.globalState.default_font_name, 90)

        self.go_to_next_scene = False

        if self.next_scene is not None:
            self.next_scene.resetState(globalState)

    def onEvent(self, event):
        if event.type == pygame.KEYUP or event.type == pygame.MOUSEBUTTONUP:
            self.go_to_next_scene = True

    def onFrame(self):
        if self.go_to_next_scene and self.next_scene is not None:
            self.globalState.display.scene = self.next_scene

    def draw(self, surface):
        """Draw all the UI components"""

        self.obj_message = self.font.render(
            "Press any key to continue", True, self.globalState.color_fg
        )
        self.obj_message_rect = self.obj_message.get_rect()
        self.obj_message_rect.topleft = (
            self.globalState.window_width - self.obj_message_rect.width - 20,
            self.globalState.window_height - self.obj_message_rect.height - 10,
        )
        surface.blit(self.obj_message, self.obj_message_rect)

        if self.title_message:
            obj_message_title = self.title_font.render(
                self.title_message, True, self.globalState.color_fg
            )
            obj_message_title_rect = obj_message_title.get_rect()
            obj_message_title_rect.topleft = (
                int(
                    (self.globalState.window_width / 2)
                    - (obj_message_title_rect.width / 2)
                ),
                int(
                    (self.globalState.window_height / 2)
                    - (obj_message_title_rect.height / 2)
                ),
            )
            surface.blit(obj_message_title, obj_message_title_rect)

        if self.message:
            for i, line in enumerate(self.message.splitlines()):
                obj_message_left = self.font.render(
                    line, True, self.globalState.color_fg
                )
                obj_message_left_rect = obj_message_left.get_rect()
                obj_message_left_rect.topleft = (
                    25,
                    int(
                        (self.globalState.window_height * 0.75)
                        - (obj_message_left_rect.height / 2)
                        + (obj_message_left_rect.height + 3) * i
                    ),
                )
                surface.blit(obj_message_left, obj_message_left_rect)


class Display:
    def __init__(self, **kwargs):
        self.globalState = (
            kwargs["global_state"] in kwargs
            if "global_state" in kwargs
            else GlobalState()
        )
        self.globalState.display = self
        self.scene = (
            kwargs["scene"]
            if "scene" in kwargs
            else SimpleTitleScene(
                next_scene=None,
            )
        )
        self.scene.resetState(self.globalState)

        self.clock = pygame.time.Clock()
        self.surface = pygame.display.set_mode(
            (self.globalState.window_width, self.globalState.window_height)
        )

    def frame(self):
        # Collect input events
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (
                event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE
            ):
                sys.exit(0)

            # Execute event receiver of the current scene
            self.scene.onEvent(event)

        # Clear scene frame with a solid color
        self.surface.fill(self.globalState.color_bg)

        # Execute onFrame() method of the current scene
        self.scene.onFrame()

        # Execute draw() method of the current scene
        self.scene.draw(self.surface)

        # Display the buffered frame onto the window
        pygame.display.update()

        # Synchronize the frame loop to a constant frame-per-second count
        self.clock.tick(self.globalState.fps)
