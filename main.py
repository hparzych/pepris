import pygame
from display import Display, SimpleTitleScene
from game import GameScene

title_screen_message = """
Left arrow, Right arrow moves the structure
Down arrow takes an extra step downward
Up arrow rotates the structure
Escape exits the game

"""

game_title = "Pepris."


def main():
    """Main function of the tetris-like game"""

    # Initialize Pygame library
    pygame.init()

    # Set window title
    pygame.display.set_caption(game_title)

    # Create a new display object that contains a title scene
    # that will switch into GameScene
    display = Display(
        scene=SimpleTitleScene(
            title_message=game_title,
            message=title_screen_message,
            next_scene=GameScene(),
        )
    )

    # Main window loop to update and render the game continuously
    while True:
        display.frame()


if __name__ == "__main__":
    main()
