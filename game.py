import datetime

import pygame
from pepris import Pepris

# A dictionary mapping tile kinds identifiers to their corresponding RGB colors.
TileKindToColorMap = {
    " ": (50, 50, 70),
    "R": (220, 30, 90),
    "Y": (220, 220, 30),
    "G": (50, 220, 70),
}


class GameScene:
    """Pepris game scene.

    It's a scene that displays the tetris board, accepts
    input from the user and displays current game progress.
    """

    def __init__(self, **kwargs):
        # Sum of scores from all played games in a session
        self.full_score = 0

    def resetState(self, globalState):
        # Save the global state of the app
        self.globalState = globalState

        # Core pepris engine object
        self.pepris = Pepris()

        # Display settings
        self.tileSize = 30
        self.boardOffset = (
            int(
                (
                    self.globalState.window_width
                    - self.tileSize * self.pepris.board.width
                )
                / 2
            ),
            int(
                (
                    self.globalState.window_height
                    - self.tileSize * self.pepris.board.height
                )
                / 2
            ),
        )

        self.font = pygame.font.SysFont(self.globalState.default_font_name, 24)

        # Update loop state flags
        self.frame_freeze_count = 0
        self.key_repeat_frame = 0

        self.is_score_recorded = False
        self.best_scores_text = None

    def onEvent(self, event):
        if event.type == pygame.KEYUP and self.pepris.is_finished:
            self.resetState(self.globalState)
        if event.type == pygame.KEYUP:
            self.key_repeat_frame = 0

    def save_tetris_score(self):
        with open("leaderboard.txt", "a") as f:
            f.write(
                str(self.pepris.score * 10)
                + ";"
                + datetime.datetime.now().strftime("%B %d, %Y %H:%M")
                + "\n"
            )

    def read_tetris_scores(self):
        scores = []
        with open("leaderboard.txt", "r") as f:
            for line in f:
                line = line.strip()
                columns = line.split(";")
                scores.append(
                    {
                        "points": columns[0],
                        "data": columns[1],
                    }
                )

        return scores

    def tetris_scores_text(self):
        text = "Leaderboard:\n"
        items = sorted(
            self.read_tetris_scores(), key=lambda i: int(i["points"]), reverse=True
        )
        for i, item in enumerate(items[:4]):
            text += (
                str(i + 1)
                + ". Score "
                + item["points"]
                + " points ("
                + item["data"]
                + ")\n"
            )
        return text

    def onFrame(self):
        if self.pepris.is_finished and not self.is_score_recorded:
            self.is_score_recorded = True
            self.save_tetris_score()

        if self.pepris.is_finished:
            return

        # logika odbioru zdarzenia wciśnięcia przycisku klawiatury i jego powtarzania
        keys = pygame.key.get_pressed()
        key_handled = False
        if self.key_repeat_frame == 0:
            if keys[pygame.K_LEFT]:
                self.pepris.go_left()
            if keys[pygame.K_RIGHT]:
                self.pepris.go_right()
            if keys[pygame.K_DOWN]:
                self.pepris.go_down()
            if keys[pygame.K_UP]:
                self.pepris.go_rotate()
        elif self.key_repeat_frame > 3:
            self.key_repeat_frame = -1

        if (
            keys[pygame.K_LEFT]
            or keys[pygame.K_RIGHT]
            or keys[pygame.K_DOWN]
            or keys[pygame.K_UP]
        ):
            self.key_repeat_frame += 1

        # logika aktualizacji stanu gry
        if self.frame_freeze_count > self.globalState.fps:
            self.frame_freeze_count = 0
            self.pepris.update()

        self.frame_freeze_count += 1

    def draw(self, surface):
        board = self.pepris.board
        if self.pepris.is_finished:
            surface.fill((60, 30, 40))
            if self.best_scores_text is None:
                self.best_scores_text = self.tetris_scores_text()

            text = self.best_scores_text
            for i, line in enumerate(text.splitlines()):
                obj_message_left = self.font.render(
                    line, True, self.globalState.color_fg
                )
                obj_message_left_rect = obj_message_left.get_rect()
                obj_message_left_rect.topleft = (
                    25,
                    int(
                        (self.globalState.window_height - 100)
                        - (obj_message_left_rect.height / 2)
                        + (obj_message_left_rect.height + 3) * i
                    ),
                )
                surface.blit(obj_message_left, obj_message_left_rect)

        self.draw_board(surface)
        if self.pepris.dropping_structure is not None:
            self.draw_dropping_structure(surface)

        obj_message = self.font.render(
            "Points: " + str(self.pepris.score * 10), True, self.globalState.color_fg
        )
        obj_message_rect = obj_message.get_rect()
        obj_message_rect.topleft = (
            self.globalState.window_width - obj_message_rect.width - 20,
            self.globalState.window_height - obj_message_rect.height - 10,
        )
        surface.blit(obj_message, obj_message_rect)

    def draw_tile(self, surface, tile, position):
        color = TileKindToColorMap[tile.kind.value]
        if self.pepris.is_finished and tile.kind.name == "EMPTY":
            color = (90, 50, 60)
        pygame.draw.rect(
            surface,
            color,
            pygame.Rect(
                position[0] + 2, position[1] + 2, self.tileSize - 4, self.tileSize - 4
            ),
        )

    def draw_tile_alt(self, surface, tile, position):
        pygame.draw.rect(
            surface,
            TileKindToColorMap[tile.kind.value],
            pygame.Rect(
                position[0] + 2, position[1] + 2, self.tileSize - 4, self.tileSize - 4
            ),
        )
        pygame.draw.rect(
            surface,
            self.globalState.color_bg,
            pygame.Rect(
                position[0] + 4, position[1] + 4, self.tileSize - 8, self.tileSize - 8
            ),
        )

    def draw_board(self, surface):
        tetrisBoard = self.pepris.board
        for i, row in enumerate(tetrisBoard.state):
            for j, tile in enumerate(row):
                self.draw_tile(
                    surface,
                    tile,
                    (
                        j * self.tileSize + self.boardOffset[0],
                        i * self.tileSize + self.boardOffset[1],
                    ),
                )

    def draw_dropping_structure(self, surface):
        dropping_structure = self.pepris.dropping_structure

        if dropping_structure is None:
            return

        pos_col = dropping_structure.pos_col
        pos_row = dropping_structure.pos_row
        for i, row in enumerate(dropping_structure.structure_kind.value):
            for j, value in enumerate(row):
                if value:
                    self.draw_tile_alt(
                        surface,
                        dropping_structure,
                        (
                            (j - dropping_structure.width() - 1) * self.tileSize
                            + self.boardOffset[0],
                            (i + 1) * self.tileSize + self.boardOffset[1],
                        ),
                    )
                if j + pos_col >= 0 and i + pos_row >= 0 and value:
                    self.draw_tile(
                        surface,
                        dropping_structure,
                        (
                            (j + pos_col) * self.tileSize + self.boardOffset[0],
                            (i + pos_row) * self.tileSize + self.boardOffset[1],
                        ),
                    )
