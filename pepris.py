"""Pepris engine

This file provides the engine for a Tetris-like game.
"""

import copy
import random
from enum import Enum


class PeprisTileKind(Enum):
    """Kind of a Pepris tile.

    Tile can be one of selected color or empty.
    """

    EMPTY = " "
    RED = "R"
    YELLOW = "Y"
    GREEN = "G"


class PeprisTile:
    """Pepris tiles make up a game board"""

    def __init__(self):
        self.kind = PeprisTileKind.EMPTY


class PeprisStructureKind(Enum):
    """Kind of a Pepris structure.

    Structure can be one of selected shapes."""

    L1 = [[True, False], [True, False], [True, True]]
    L2 = [[False, False, True], [True, True, True]]
    L3 = [[True, True], [False, True], [False, True]]
    L4 = [[True, True, True], [True, False, False]]
    I1 = [[True, True, True]]
    I2 = [[True], [True], [True]]
    Sq = [[True, True], [True, True]]


class PeprisBoard:
    """Board contains momentary state of Pepris structures and
    tiles on the game board."""

    def __init__(self):
        self.width = 8
        self.height = 12
        self.resetState()

    def resetState(self):
        """Empty all tiles on the board."""

        self.state = [
            [PeprisTile() for _ in range(self.width)] for _ in range(self.height)
        ]

    def is_empty_at(self, row, column):
        """Check whether a tile at location is an empty tile"""

        return (
            row < 0
            or column < 0
            or row >= self.height
            or column >= self.width
            or self.state[row][column].kind.name == "EMPTY"
        )


class PeprisStructure:
    """PeprisStructure is a shape made out of a collection of tiles."""

    def __init__(self, **kwargs):
        board_width = kwargs["board_width"]

        # Select a random color for the structure
        self.kind = random.choice(list(PeprisTileKind)[1:])

        # Select a random shape for the structure
        self.structure_kind = random.choice(list(PeprisStructureKind))

        # Set the position to top-center
        self.pos_row = self.height() * (-1)
        self.pos_col = int(board_width / 2) - int(self.width() / 2)

    def is_overlap(self, board, offset_row_col=(0, 0)):
        """Check whether the structure overlaps any non-empty tile on the board.

        offset_row_col argument offsets position of the structure in the check."""

        # Shape of the structure
        shape = self.structure_kind.value

        # Iteratively check whether tiles of the structure when positioned on the map
        # overlap with any non-empty tile
        for i in range(len(shape)):
            for j in range(len(shape[0])):
                if shape[i][j] and not board.is_empty_at(
                    i + offset_row_col[0] + self.pos_row,
                    j + offset_row_col[1] + self.pos_col,
                ):
                    return True
        return False

    def is_colliding_bottom(self, board):
        """Check whether the structure collides on the bottom side"""

        return self.pos_row >= board.height - len(
            self.structure_kind.value
        ) or self.is_overlap(board, (1, 0))

    def is_colliding_left(self, board):
        """Check whether the structure collides on the left side"""

        return self.pos_col <= 0 or self.is_overlap(board, (0, -1))

    def is_colliding_right(self, board):
        """Check whether the structure collides on the right side"""

        return self.pos_col >= board.width - len(
            self.structure_kind.value[0]
        ) or self.is_overlap(board, (0, 1))

    def write_to_board(self, board):
        """Writes the current block shape to the specified game board."""

        shape = self.structure_kind.value
        for i in range(len(shape)):
            for j in range(len(shape[0])):
                if shape[i][j] and (i + self.pos_row) >= 0:
                    board.state[i + self.pos_row][j + self.pos_col].kind = self.kind

    def width(self):
        """Returns width of the board"""
        return len(self.structure_kind.value[0])

    def height(self):
        """Returns height of the board"""
        return len(self.structure_kind.value)

    # A dictionary that maps block kinds to their corresponding clockwise rotation.
    kindRotationMap = {
        "L1": "L2",  # L-shaped structure rotation 1 to rotation 2
        "L2": "L3",  # L-shaped structure rotation 2 to rotation 3
        "L3": "L4",  # L-shaped structure rotation 3 to rotation 4
        "L4": "L1",  # L-shaped structure rotation 4 to rotation 1
        "Sq": "Sq",  # Square structure does not rotate
        "I1": "I2",  # I-shaped structure rotation 1 to rotation 2
        "I2": "I1",  # I-shaped structure rotation 2 to rotation 1
    }


class PeprisEventKind(Enum):
    Void = 0
    Updated = 1
    LevelFinished = 2


def is_row_filled(row):
    for col in row:
        if col.kind.name == "EMPTY":
            return False
    return True


class Pepris:
    def __init__(self):
        # Create a new board
        self.board = PeprisBoard()

        # There is no currently dropping structure on start
        self.dropping_structure = None

        #
        self.update_task_check_winning = False

        # Flag that is true when the game is in a finished state
        self.is_finished = False

        # Game score
        self.score = 0

    def update(self):
        """Update the game state."""

        if self.is_finished:
            return

        if self.update_task_check_winning:
            self.delete_winning_rows()
            self.update_task_check_winning = False
            return

        if self.dropping_structure is None:
            self.dropping_structure = PeprisStructure(board_width=self.board.width)
            if self.dropping_structure.is_overlap(
                self.board
            ) or self.dropping_structure.is_colliding_bottom(self.board):
                self.is_finished = True
                return

        self.go_down()
        self.check_and_write_dropping_structure()

    def check_and_write_dropping_structure(self):
        if (
            self.dropping_structure is not None
            and self.dropping_structure.is_colliding_bottom(self.board)
        ):
            self.dropping_structure.write_to_board(self.board)
            self.dropping_structure = None
            self.update_task_check_winning = True
            if random.choice([True, False]):
                self.score += 1
            elif random.choice([True, False]):
                self.score += 2

    def delete_winning_rows(self):
        """Delete rows that are winning aka all it's tiles are non-empty."""

        rows = self.board.state
        if is_row_filled(rows[0]):
            return

        # Structure gravitation logic
        i = 1
        while i < len(rows):
            if is_row_filled(rows[i]):
                # Increase score in a random way
                if random.choice([True, False]):
                    self.score = int(self.score * 1.25 + 15)
                else:
                    self.score = int(self.score * 1.25 + 10)

                i2 = i
                while i2 > 0:
                    rows[i2] = rows[i2 - 1]
                    i2 -= 1
                rows[0] = []
                for m in range(self.board.width):
                    rows[0].append(PeprisTile())

            i += 1

    def go_left(self):
        """Move currently dropping structure left."""

        if (
            self.dropping_structure is None
            or self.dropping_structure.is_colliding_left(self.board)
        ):
            return
        self.dropping_structure.pos_col -= 1

    def go_right(self):
        """Move currently dropping structure right."""

        if (
            self.dropping_structure is None
            or self.dropping_structure.is_colliding_right(self.board)
        ):
            return
        self.dropping_structure.pos_col += 1

    def go_down(self):
        """Move currently dropping structure down."""
        if (
            self.dropping_structure is not None
            and not self.dropping_structure.is_colliding_bottom(self.board)
        ):
            self.dropping_structure.pos_row += 1

    def go_rotate(self):
        """Rotate currently dropping structure."""

        if self.dropping_structure is None:
            return

        target = copy.deepcopy(self.dropping_structure)
        target.structure_kind = PeprisStructureKind[
            PeprisStructure.kindRotationMap[target.structure_kind.name]
        ]

        offset_bottom_board = self.board.height - target.pos_row - target.height() + 1
        if offset_bottom_board < 0:
            target.pos_row += offset_bottom_board

        offset_right = self.board.width - target.pos_col - target.width()
        if offset_right < 0:
            target.pos_col += offset_right

        offset_left = target.pos_col
        if offset_left < 0:
            target.pos_col -= offset_left

        if self.dropping_structure.is_overlap(self.board):
            return

        self.dropping_structure = target
